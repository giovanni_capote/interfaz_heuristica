#include <stdio.h>
#define tam 50

int main()
{
    int Valor[tam];
    int i=0, Suma=0;
    printf("\t----Interfaz de evaluacion heurística----\n");
    printf("Introducción de valores en criterios recogidos por Nielsen como estándar\n");
    printf("Por favor introduzca valores enteros en un rango del 0 al 5, como 5 siendo el mayor puntaje y 0 el menor puntaje\n");
    printf("\n\t1) Visibilidad del estado del sistema\n");
    printf("El sistema siempre debería mantener informados a los usuarios de lo que está ocurriendo, a través de retroalimentación apropiada dentro de un tiempo razonable: ");
    scanf("%d",&Valor[i]);
    while(Valor[i]<0 || Valor[i]>5){
        printf("Ingrese un valor aceptado entre el 1 al 5: ");
        scanf("%d",&Valor[i]);
    }
    i++;
    printf("\n\t2) Relación entre el sistema y el mundo real\n");
    printf("El sistema debería hablar el lenguaje de los usuarios mediante palabras, frases y conceptos que sean familiares al usuario, más que con términos relacionados con el sistema: ");
    scanf("%d",&Valor[i]);
    while(Valor[i]<0 || Valor[i]>5){
        printf("Ingrese un valor aceptado entre el 1 al 5:" );
        scanf("%d",&Valor[i]);
    }
    i++;
    printf("\n\t3) Control y libertad para el usuario\n");
    printf("Los usuarios elegirán las funciones del sistema por error y necesitarán una “salida de emergencia” claramente marcada para dejar el estado no deseado al que accedieron, sin tener que pasar por una serie de pasos: ");
    scanf("%d",&Valor[i]);
    while(Valor[i]<0 || Valor[i]>5){
        printf("Ingrese un valor aceptado entre el 1 al 5: ");
        scanf("%d",&Valor[i]);
    }
    i++;
    printf("\n\t4) Consistencia y estándares\n");
    printf("Los usuarios no deberían cuestionarse si acciones, situaciones o palabras diferentes significan en realidad la misma cosa: ");
    scanf("%d",&Valor[i]);
    while(Valor[i]<0 || Valor[i]>5){
        printf("Ingrese un valor aceptado entre el 1 al 5: ");
        scanf("%d",&Valor[i]);
    }
    i++;
    printf("\n\t5) Prevención de errores\n");
    printf("Un buen diseño de mensajes de error es realizar un diseño cuidadoso que prevenga la ocurrencia de problemas: ");
    scanf("%d",&Valor[i]);
    while(Valor[i]<0 || Valor[i]>5){
        printf("Ingrese un valor aceptado entre el 1 al 5: ");
        scanf("%d",&Valor[i]);
    }
    i++;
    printf("\n\t6) Reconocimiento antes que recuerdo\n");
    printf("El usuario no tendría que recordar la información que se le da en una parte del proceso, para seguir adelante: ");
    scanf("%d",&Valor[i]);
    while(Valor[i]<0 || Valor[i]>5){
        printf("Ingrese un valor aceptado entre el 1 al 5: ");
        scanf("%d",&Valor[i]);
    }
    i++;
    printf("\n\t7) Flexibilidad y eficiencia de uso\n");
    printf("La presencia de aceleradores, que no son vistos por los usuarios novatos, puede ofrecer una interacción más rápida a los usuarios expertos que la que el sistema puede proveer a los usuarios de todo tipo: ");
    scanf("%d",&Valor[i]);
    while(Valor[i]<0 || Valor[i]>5){
        printf("Ingrese un valor aceptado entre el 1 al 5: ");
        scanf("%d",&Valor[i]);
    }
    i++;
    printf("\n\t8) Estética y diseño minimalista\n");
    printf("Los diálogos no deben contener información que es irrelevante o poco usada: ");
    scanf("%d",&Valor[i]);
    while(Valor[i]<0 || Valor[i]>5){
        printf("Ingrese un valor aceptado entre el 1 al 5: ");
        scanf("%d",&Valor[i]);
    }
    i++;
    printf("\n\t9) Ayudar a los usuarios a reconocer\n");
    printf("Los mensajes de error se deben entregar en un lenguaje claro y simple, indicando en forma precisa el problema y sugerir una solución constructiva al problema: ");
    scanf("%d",&Valor[i]);
    while(Valor[i]<0 || Valor[i]>5){
        printf("Ingrese un valor aceptado entre el 1 al 5: ");
        scanf("%d",&Valor[i]);
    }
    i++;
    printf("\n\t10) Ayuda y documentación\n");
    printf("Incluso en los casos en que el sistema pueda ser usado sin documentación, podría ser necesario ofrecer ayuda y documentación: ");
    scanf("%d",&Valor[i]);
    while(Valor[i]<0 || Valor[i]>5){
        printf("Ingrese un valor aceptado entre el 1 al 5: ");
        scanf("%d",&Valor[i]);
    }
    i++;
    i==0;
    printf("\n\n----Resumen----\n");
    printf("\n1) Visibilidad del estado del sistema: %d",Valor[i]);
    printf("\n--Comentarios--\n");
    if(Valor[i]==5){
        printf("La Visibilidad del estado del sistema ha sido correctamente implementada.");
    }
    else if(Valor[i]==3 || Valor[i]==4){
        printf("Se muestra un nivel adecuado de Visibilidad del estado del sistema, sin embargo, se puede implementar una descripción mejor detallada y con un lapso prudente de tiempo.");
    }
    else if(Valor[i]==1 || Valor[i]==2){
        printf("La Visibilidad del estado del sistema se encuentra en un nivel básico o casi nulo, se puede corregir el lapso de tiempo prudente para que aparezca un mensaje descriptivo sobre el sistema.");
    }
    else{
        printf("No existe ningún atributo que ayude al usuario con la Visibilidad delo estado del sistema.");
    }
    i++;
    printf("\n\n2) Relación entre el sistema y el mundo real: %d",Valor[i]);
    printf("\n--Comentarios--\n");
    if(Valor[i]==5){
        printf("La Relación entre el sistema y el mundo real ha sido correctamente implementada.");
    }
    else if(Valor[i]==3 || Valor[i]==4){
        printf("Se muestra un nivel adecuado de Relación entre el sistema y el mundo real, sin embargo, se puede mejorar la aparición de la información siguiendo un orden más natural y lógico.");
    }
    else if(Valor[i]==1 || Valor[i]==2){
        printf("La Relación entre el sistema y el mundo real se encuentra en un nivel básico o casi nulo, se puede corregir la interacción al seguir las convenciones del mundo real, haciendo que la información aparezca en un orden natural y lógico.");
    }
    else{
        printf("No existe ningún atributo que ayude al usuario con la Relación entre el sistema y el mundo real");
    }
    i++;
    printf("\n\n3) Control y libertad para el usuario: %d",Valor[i]);
    printf("\n--Comentarios--\n");
    if(Valor[i]==5){
        printf("El Control y libertad para el usuario ha sido correctamente implementado.");
    }
    else if(Valor[i]==3 || Valor[i]==4){
        printf("Se muestra un nivel adecuado de Control y libertad para el usuario, sin embargo, se puede mejorar la aparición de la información siguiendo un orden más natural y lógico.");
    }
    else if(Valor[i]==1 || Valor[i]==2){
        printf("La Relación entre el sistema y el mundo real se encuentra en un nivel básico o casi nulo, se puede corregir la interacción al seguir las convenciones del mundo real, haciendo que la información aparezca en un orden natural y lógico.");
    }
    else{
        printf("No existe ningún atributo que ayude al usuario con la Relación entre el sistema y el mundo real");
    }
    i++;
    printf("\n\n4) Consistencia y estándares: %d",Valor[i]);
    printf("\n--Comentarios--\n");
    if(Valor[i]==5){
        printf("La Consistencia y estándares para el usuario ha sido correctamente implementado.");
    }
    else if(Valor[i]==3 || Valor[i]==4){
        printf("Se muestra un nivel adecuado de Consistencia y estándares para el usuario, sin embargo, se puede mejorar siguiendo los estándares convencionales.");
    }
    else if(Valor[i]==1 || Valor[i]==2){
        printf("La Relación entre el sistema y el mundo real se encuentra en un nivel básico o casi nulo, se puede corregir la interacción apegándose a las convenciones ya establecidas.");
    }
    else{
        printf("No existe ningún atributo que ayude al usuario con la Consistencia y estándares");
    i++;
    printf("\n\n5) Prevención de errores: %d",Valor[i]);
    printf("\n--Comentarios--\n");
    if(Valor[i]==5){
        printf("La Prevención de errores para el usuario ha sido correctamente implementado.");
    }
    else if(Valor[i]==3 || Valor[i]==4){
        printf("Se muestra un nivel adecuado de Prevención de errores para el usuario, sin embargo, se puede mejorar mediante más implementaciones que ayude al usuario a no caer en errores.");
    }
    else if(Valor[i]==1 || Valor[i]==2){
        printf("La Prevención de errores se encuentra en un nivel básico o casi nulo, se puede corregir mediante la realización un diseño cuidadoso que prevenga la ocurrencia de problemas.");
    }
    else{
        printf("No existe ningún atributo que ayude al usuario con la Prevención de errores");
    i++;
    printf("\n\n6) Reconocimiento antes que recuerdo: %d",Valor[i]);
    printf("\n--Comentarios--\n");
    if(Valor[i]==5){
        printf("El Reconocimiento antes que recuerdo para el usuario ha sido correctamente implementado.");
    }
    else if(Valor[i]==3 || Valor[i]==4){
        printf("Se muestra un nivel adecuado de Reconocimiento antes que recuerdo para el usuario, sin embargo, se puede mejorar a que el usuario no tendría que recordar la información que se le da en una parte del proceso.");
    }
    else if(Valor[i]==1 || Valor[i]==2){
        printf("La Prevención de errores se encuentra en un nivel básico o casi nulo, se puede corregir mediante instrucciones para el uso del sistema deben estar a la vista o ser fácilmente recuperables cuando sea necesario.");
    }
    else{
        printf("No existe ningún atributo que ayude al usuario con el Reconocimiento antes que recuerdo");
    i++;
    printf("\n\n7) Flexibilidad y eficiencia de uso: %d",Valor[i]);
    printf("\n--Comentarios--\n");
    if(Valor[i]==5){
        printf("La Flexibilidad y eficiencia de uso para el usuario ha sido correctamente implementado.");
    }
    else if(Valor[i]==3 || Valor[i]==4){
        printf("Se muestra un nivel adecuado de Flexibilidad y eficiencia de uso para el usuario, sin embargo, se debe permitir que los usuarios adapten el sistema para usos frecuentes.");
    }
    else if(Valor[i]==1 || Valor[i]==2){
        printf("La Flexibilidad y eficiencia se encuentra en un nivel básico o casi nulo, se puede corregir mediante a presencia de aceleradores, que no son vistos por los usuarios novatos, puede ofrecer una interacción más rápida a los usuarios expertos que la que el sistema puede proveer a los usuarios de todo tipo.");
    }
    else{
        printf("No existe ningún atributo que ayude al usuario con la Flexibilidad y eficiencia");
    i++;
    printf("\n\n8) Estética y diseño minimalista: %d",Valor[i]);
    printf("\n--Comentarios--\n");
    if(Valor[i]==5){
        printf("La Estética y diseño minimalista de uso para el usuario ha sido correctamente implementado.");
    }
    else if(Valor[i]==3 || Valor[i]==4){
        printf("Se muestra un nivel adecuado de Estética y diseño minimalista de uso para el usuario, sin embargo, los diálogos no deben contener información que es irrelevante o poco usada");
    }
    else if(Valor[i]==1 || Valor[i]==2){
        printf("La Estética y diseño minimalista se encuentra en un nivel básico o casi nulo, se puede corregir sabiendo que cada unidad extra de información en un diálogo, compite con las unidades de información relevante y disminuye su visibilidad relativa.");
    }
    else{
        printf("No existe ningún atributo que ayude al usuario con la Flexibilidad y eficiencia");
    i++;
    printf("\n\n9) Ayudar a los usuarios a reconocer: %d",Valor[i]);
    printf("\n--Comentarios--\n");
    if(Valor[i]==5){
        printf("El Ayudar a los usuarios a reconocer de uso para el usuario ha sido correctamente implementado.");
    }
    else if(Valor[i]==3 || Valor[i]==4){
        printf("Se muestra un nivel adecuado de Ayudar a los usuarios a reconocer, sin embargo, se debe ayudar a diagnosticar y recuperarse de errores");
    }
    else if(Valor[i]==1 || Valor[i]==2){
        printf("El Ayudar a los usuarios a reconocer se encuentra en un nivel básico o casi nulo, se puede corregir al saber que los mensajes de error se deben entregar en un lenguaje claro y simple, indicando en forma precisa el problema y sugerir una solución constructiva al problema.");
    }
    else{
        printf("No existe ningún atributo que ayude al usuario con el Ayudar a los usuarios a reconocer");
    i++;
    printf("\n\n10) Ayuda y documentación: %d",Valor[i]);
    printf("\n--Comentarios--\n");
    if(Valor[i]==5){
        printf("La Ayuda y documentación para el usuario ha sido correctamente implementado.");
    }
    else if(Valor[i]==3 || Valor[i]==4){
        printf("Se muestra un nivel adecuado de Ayuda y documentación, sin embargo, incluso en los casos en que el sistema pueda ser usado sin documentación, podría ser necesario ofrecer ayuda y documentación");
    }
    else if(Valor[i]==1 || Valor[i]==2){
        printf("La Ayuda y documentación se encuentra en un nivel básico o casi nulo, se puede corregir ya que dicha información debería ser fácil de buscar, estar enfocada en las tareas del usuario, con una lista concreta de pasos a desarrollar y no ser demasiado extensa.");
    }
    else{
        printf("No existe ningún atributo que ayude al usuario con la Ayuda y documentación");
    i==0;
    while(i<=10){
        Suma+=Valor[i];
        i++;
    }
    printf("\n\nSuma total de valores introducidos a los criterios heurísticos: %d",Suma);
    printf("\n--Comentarios--\n");
    if(Suma=50){
        printf("Se puede considerar al sistema ideal, mediante los criterios heurísticos.");
    }
    else if(Suma>25 || Suma<50){
        printf("Se muestra un nivel adecuado dentro de la estructura del sistema, se puede mejorar mediante los consejos mostrados en cada criterio heurístico.");
    }
    else if(Suma>0 || Suma<25){
        printf("La idealidad del sistema se encuentra en un nivel básico o casi nulo, se puede corregir al implementar las correciones brindadas en cada criterio heurístico.");
    }
    else{
        printf("No existe ningún atributo que califique al sistema como un sistema ideal");
    }
}
}
}
}
}
}
}
}






